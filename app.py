#! /usr/bin/python
# -*- coding:utf-8 -*-

from flask import Flask, render_template, flash, redirect, request
from forms import LoginForm
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from data import Articles

Articles = Articles()
app = Flask(__name__)

@app.route('/')
def index():
    user = {'name': 'Julien'}
    posts = [
        {
            'author': {'username': 'John'},
            'body': 'Wenger OUT of this!'
        },
        {
            'author': {'username': 'Julien'},
            'body': 'We know the end.'
        },
        {
            'author': {'username': 'JulGoTheKey'},
            'body': 'I got the key'
        }
    ]
    return render_template('home.html', title='Home', user=user, posts=posts)

@app.route('/login', methods=['GET', 'POST'])
def login():
    user = {'name': 'Julien'}
    form = LoginForm()
    if form.validate_on_submit():
        flash('login requested for user {}, remember_me={}'.format(
            form.username.data, form.remember_me.data))
        return redirect('/')
    return render_template('login.html', title='Sign In', user=user, form=form)

@app.route('/articles')
def articles():
    user = {'name': 'Julien'}
    return render_template('articles.html', user= user, articles= Articles)

@app.route('/article/<string:id>/')
def article(id):
    user = {'name': 'Julien'}
    return render_template('article.html', user= user, id= id)

@app.route('/article/createArticle/', methods=['GET', 'POST'])
def createArticle():
    message = ''
    if request.method == 'POST':
        message = request.form['message']
    return render_template('create_article.html', message= message)

if __name__ == '__main__':
    app.config.from_object(Config)
    db = SQLAlchemy(app)
    migrate = Migrate(app, db)
    app.secret_key = '2frVe)f&é,A$p@pzk+z*U03êû9_'
    app.run(debug=True, port=5002)


