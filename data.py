def Articles():
    articles = [
        {
            'id': 1,
            'title': 'Article One',
            'body': 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores ex qui odio, aspernatur consequuntur eveniet vitae ullam temporibus quas tempora et minima eos corrupti odit sequi quos, cumque possimus repudiandae.',
            'author': 'JulGoTheName',
            'created_date': '03-12-2015'
        },
        {
            'id': 2,
            'title': 'Article Tow',
            'body': 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores ex qui odio, aspernatur consequuntur eveniet vitae ullam temporibus quas tempora et minima eos corrupti odit sequi quos, cumque possimus repudiandae.',
            'author': 'JulGoTheName',
            'created_date': '03-12-2015'
        },
        {
            'id': 3,
            'title': 'Article Three',
            'body': 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores ex qui odio, aspernatur consequuntur eveniet vitae ullam temporibus quas tempora et minima eos corrupti odit sequi quos, cumque possimus repudiandae.',
            'author': 'JulGoTheName',
            'created_date': '03-12-2015'
        },        
    ]
    return articles